const express = require("express");
const router = express.Router();

const auth = require("../auth.js");
const userController = require("../controllers/userController.js");

// User registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// User authentication (login)
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Retrieve user details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getUserDetails({ userId : userData.id }).then(resultFromController => res.send(resultFromController));
})

// *Set user as admin
router.patch("/admin", auth.verify, (req, res) => { 

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		userController.setToAdmin(req.body).then((resultFromController) => res.send(resultFromController));
	} else {
		console.log({ auth : "unauthorized user" });

		res.send(false);
	}
})

// ~Remove user admin access
router.patch("/", auth.verify, (req, res) => { 

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		userController.removeAdmin(req.body).then((resultFromController) => res.send(resultFromController));
	} else {
		console.log({ auth : "unauthorized user" });

		res.send(false);
	}
})

// ~Update user details
router.patch("/details", auth.verify, (req, res) => { 

	const email = auth.decode(req.headers.authorization).email;
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	const userData = auth.decode(req.headers.authorization);

	if(email === req.body.email && !isAdmin) {
		userController.updateUserDetails({ userId : userData.id }, req.body).then((resultFromController) => res.send(resultFromController));
	} else {
		console.log({ auth : "no access to this user details" });

		res.send(false);
	}
})

// ~Get all users
router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin === true) {
		userController.getAllUsers().then((resultFromController) => res.send(resultFromController));
	} else {
		console.log({ auth : "unauthorized user" });

		res.send("User not an admin.");
	}	
})

// ~Get specific user
router.post("/user", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin === true) {
		userController.getSpecificUser(req.body.id).then((resultFromController) => res.send(resultFromController));
	} else {
		console.log({ auth : "unauthorized user" });

		res.send("User not an admin.");
	}	
})





module.exports = router;