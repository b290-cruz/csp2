const express = require("express");
const router = express.Router();

const auth = require("../auth.js");
const productController = require("../controllers/productController.js");

// Create product
router.post("/create", auth.verify, (req, res) => { 

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		productController.createProduct(req.body).then((resultFromController) => res.send(resultFromController));
	} else {
		console.log({ auth : "unauthorized user" });

		res.send(false);
	}
})

// Retrieve all products
router.get("/all", auth.verify, (req, res) => { 

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		productController.getAllProducts().then((resultFromController) => res.send(resultFromController));
	} else {
		console.log({ auth : "unauthorized user" });

		res.send(false);
	}
})

// Retrieve all active products
router.get("/active", (req, res) => { 

	productController.getAllActiveProducts().then((resultFromController) => res.send(resultFromController));
})

// Retrieve single product
router.get("/:productId", (req, res) => { 

	productController.getProduct(req.params).then((resultFromController) => res.send(resultFromController));
})

// Update product information
router.put("/:productId", auth.verify, (req, res) => { 

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		productController.updateProduct(req.params, req.body).then((resultFromController) => res.send(resultFromController));
	} else {
		console.log({ auth : "unauthorized user" });

		res.send(false);
	}
})

// Archive product
router.patch("/:productId/archive", auth.verify, (req, res) => { 

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		productController.archiveProduct(req.params, req.body).then((resultFromController) => res.send(resultFromController));
	} else {
		console.log({ auth : "unauthorized user" });

		res.send(false);
	}
})

// Activate product
router.patch("/:productId", auth.verify, (req, res) => { 

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		productController.activateProduct(req.params, req.body).then((resultFromController) => res.send(resultFromController));
	} else {
		console.log({ auth : "unauthorized user" });

		res.send(false);
	}
})





module.exports = router;