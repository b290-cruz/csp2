const express = require("express");
const router = express.Router();

const auth = require("../auth.js");
const orderController = require("../controllers/orderController.js");

// Non-admin user checkout
router.post("/checkout", auth.verify, (req, res) => { 

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		console.log({ auth : "unauthorized user" });

		res.send(false);
	} else {
		let data = {
			userId : auth.decode(req.headers.authorization).id,
			productId : req.body.productId,
			quantity : req.body.quantity
		};

		orderController.createOrder(data).then(resultFromController => res.send(resultFromController));
	}
})

// *Retrieve authenticated user's orders
router.get("/done", auth.verify, (req, res) => { 

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		console.log({ auth : "unauthorized user" });

		res.send(false);
	} else {

		const userData = auth.decode(req.headers.authorization);

		orderController.getUserOrders({ userId : userData.id }).then(resultFromController => res.send(resultFromController));
	}
})

// *Retrieve all orders
router.get("/all", auth.verify, (req, res) => { 

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {

		orderController.getAllOrders().then(resultFromController => res.send(resultFromController));
	} else {

		console.log({ auth : "unauthorized user" });

		res.send(false);
	}
})

// ~Order cancellation
router.patch("/:orderId/cancel", auth.verify, (req, res) => { 

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		console.log({ auth : "unauthorized user" });

		res.send(false);
	} else {

		orderController.cancelOrder(req.params, req.body).then(resultFromController => res.send(resultFromController));
	}
})

// ~Order completion
router.patch("/:orderId/done", auth.verify, (req, res) => { 

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		console.log({ auth : "unauthorized user" });

		res.send(false);
	} else {

		orderController.completeOrder(req.params, req.body).then(resultFromController => res.send(resultFromController));
	}
})

// ~Retrieve product sales
router.get("/:productId/sales", auth.verify, (req, res) => { 

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {

		orderController.getProductSales(req.params).then(resultFromController => res.send(resultFromController));
	} else {

		console.log({ auth : "unauthorized user" });

		res.send(false);
	}
})



/* ======= STRETCH GOALS : CART ======= */

// *Add to cart
// Should only be for authenticated non-admin user
router.post("/:productId", auth.verify, (req, res) => { 

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		console.log({ auth : "unauthorized user" });

		res.send(false);
	} else {

		const userId = auth.decode(req.headers.authorization).id;

		orderController.addToCart(userId, req.params.productId, req.body.quantity).then(resultFromController => res.send(resultFromController));
	}
})

// *Added products (Retrieve all products from cart)
// Should only be for authenticated non-admin user
router.get("/cart", auth.verify, (req, res) => { 

	try {
        
        const isAdmin = auth.decode(req.headers.authorization).isAdmin;

        if(isAdmin) {
        	console.log({ auth : "unauthorized user" });
        	res.send(false);
        } else {

        	const userId = auth.decode(req.headers.authorization).id;

        	orderController.getCart(userId).then(resultFromController => res.send(resultFromController));
        }
    } catch (err) {

        console.log(err)
    }
})

// *Change product quantities and Show(Subtotal for each item & Total price for all items)
// Should only be for authenticated non-admin user
router.patch("/cart/:productId", auth.verify, (req, res) => { 

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		console.log({ auth : "unauthorized user" });

		res.send(false);
	} else {

		const userId = auth.decode(req.headers.authorization).id;

		orderController.editCart(userId, req.params.productId, req.body.quantity).then(resultFromController => res.send(resultFromController));
	}
})

// *Remove products from cart
// Should only be for authenticated non-admin user
router.patch("/cart/:productId/remove", auth.verify, (req, res) => { 

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		console.log({ auth : "unauthorized user" });

		res.send(false);
	} else {

		const userId = auth.decode(req.headers.authorization).id;

		orderController.removeFromCart(userId, req.params.productId).then(resultFromController => res.send(resultFromController));
	}
})

// ~Non-admin user cart to checkout 
// Should only be for authenticated non-admin user
router.patch("/cartcheckout", auth.verify, (req, res) => { 

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		console.log({ auth : "unauthorized user" });

		res.send(false);
	} else {

		const userId = auth.decode(req.headers.authorization).id;

		orderController.cartToOrder(userId).then(resultFromController => res.send(resultFromController));
	}
})
/* ======= END OF STRETCH GOALS : CART ======= */





module.exports = router;