const mongoose = require("mongoose");

const orderSchema  = new mongoose.Schema(
	{
		userId : { // User who owns the order (connected to User.js)
			type : String,
			required : [true, "UserId is required"]
		},
		products : [
			{
				productId : { // Product being bought (connected to Product.js)
					type : String,
					required : [true, "UserId is required"]
				},
				quantity : {
					type : Number,
					required : [true, "quantity is required"]
				},
				subTotal : Number
			}
		],
		totalAmount : Number,
		purchasedOn : {
			type : Date,
			default : new Date()
		},
		status : {
			type : String,
			default : "Pending"
		}
	}
);

module.exports = mongoose.model("Order", orderSchema);