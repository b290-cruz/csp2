// Import bcrypt package for password encryption
const bcrypt = require("bcrypt");

// JS file with JWT/JSONWebToken for user authentication
const auth = require("../auth.js");

const Order = require("../models/Order.js");
const Product = require("../models/Product.js");
const User = require("../models/User.js");

// For ObjectId
const {ObjectId} = require('mongodb');

// User registration
module.exports.registerUser = reqBody => {

	return User.find({ email : reqBody.email }).then(data => {

		if(data.length > 0){			
			console.log("Email already registered!");
			return false;			
		} else {

			let newUser = new User({		
				firstName : reqBody.firstName,
				lastName : reqBody.lastName,
				mobileNumber : reqBody.mobileNumber,
				completeAddress : reqBody.completeAddress,
				email : reqBody.email,
				password : bcrypt.hashSync(reqBody.password, 10)	
			})

			return newUser.save().then(user => true).catch(err => {
				console.log(err);

				return false;
			})
		}
	}).catch(err => {
		console.log(err);

		return false;
	});
};

// User authentication (login)
module.exports.loginUser = reqBody => {

	return User.findOne({ email : reqBody.email }).then(result => {

		if(result == null) { // User does not exist

			console.log("User not registered");

			return false; // User is not registered
		} else { // User exists

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){ // Passwords match, truthy
				return { access : auth.createAccessToken(result) }
			} else {

				console.log("Password did not match");

				return false // Password did not match
			}
		}
	}).catch(err => {
		console.log(err);

		return false;
	});
};

// Retrieve user details
module.exports.getUserDetails = (data) => {

	return User.findById(data.userId).then(result => {

		result.password = "";

		return result
	}).catch(err => {
		console.log(err);

		return false;
	});
};

// *Set user as admin
module.exports.setToAdmin = reqBody => { 
	
	return User.findOne({ email : reqBody.email }).then(data => {

		if(data.isAdmin === false){	

			let toAdmin = {
				isAdmin : reqBody.isAdmin
			};

			return User.findOneAndUpdate({ email : reqBody.email },
				toAdmin)
				.then(user => {

					console.log("User set to admin.")
					return true;
				}).catch(err => {
					console.log(err);

					return false;
				})		
		} else {		
			console.log("User already an admin.")
			return false
		}
	}).catch(err => {
		console.log(err);

		return false;
	})
};

// ~Remove user admin access
module.exports.removeAdmin = reqBody => { 
	
	return User.findOne({ email : reqBody.email }).then(data => {

		if(data.isAdmin === false){	
			console.log("User already an non-admin.")
			return false		
		} else {		

			let toAdmin = {
				isAdmin : reqBody.isAdmin
			};

			return User.findOneAndUpdate({ email : reqBody.email },
				toAdmin)
				.then(user => {

					console.log("User set to non-admin.")
					return true;
				}).catch(err => {
					console.log(err);

					return false;
				})
		}
	}).catch(err => {
		console.log(err);

		return false;
	})
};

// ~Update user details
module.exports.updateUserDetails = (data, reqBody) => { 

	let updatedUser = {};

	if (data.password === "" || data.password === null || data.password === undefined) {
		updatedUser = {
			firstName : reqBody.firstName,
			lastName : reqBody.lastName,
			mobileNumber : reqBody.mobileNumber,
			completeAddress : reqBody.completeAddress,
			email : reqBody.email
		};
	} else {
		updatedUser = {
			firstName : reqBody.firstName,
			lastName : reqBody.lastName,
			mobileNumber : reqBody.mobileNumber,
			completeAddress : reqBody.completeAddress,
			email : reqBody.email,
			password: bcrypt.hashSync(reqBody.password, 10)
		};
	}

	return User.findByIdAndUpdate(data.userId, updatedUser)
		.then(user => {

			console.log("User details updated.");

			return true;
		}).catch(err => {
			console.log(err);

			return false;
		})
};

// ~Get all users
module.exports.getAllUsers = reqBody => {

	return User.find({}).then(data => {

		return data;
	}).catch(err => {
		console.log(err);

		return false;
	})
};

// ~Get specific user
module.exports.getSpecificUser = userId => {

	return User.findById(userId).then(data => {

		data.password = "";

		return data;
	}).catch(err => {
		console.log(err);

		return false;
	})
};