// JS file with JWT/JSONWebToken for user authentication
const auth = require("../auth.js");

const Order = require("../models/Order.js");
const Product = require("../models/Product.js");
const User = require("../models/User.js");

// Create product
module.exports.createProduct = reqBody => { 

	console.log(reqBody)

	return Product.find({ sku : reqBody.sku }).then(data => {

		if(data.length > 0){			
			console.log("Product already in the system!");
			return false;			
		} else {

			let newProduct = new Product({		
				name : reqBody.name, 
				description : reqBody.description,
				brand : reqBody.brand,
				sku : reqBody.sku,
				stocks : reqBody.stocks,
				price : reqBody.price
			})

			return newProduct.save().then(user => true).catch(err => {
				console.log(err);

				return false;
			})
		}
	}).catch(err => {
		console.log(err);

		return false;
	});
};

// Retrieve all products
module.exports.getAllProducts = reqBody => { 

	return Product.find({}).then(data => {

		return data;
	}).catch(err => {
		console.log(err);

		return false;
	})
};

// Retrieve all active products
module.exports.getAllActiveProducts = reqBody => { 

	return Product.find({ isActive : true }).then(data => {

		return data;
	}).catch(err => {
		console.log(err);

		return false;
	})
};

// Retrieve single product
module.exports.getProduct = reqParams => { 

	return Product.findById(reqParams.productId).then(data => {

		return data;
	}).catch(err => {
		console.log(err);

		return false;
	})	
};

// Update product information
module.exports.updateProduct = (reqParams, reqBody) => { 

	let updatedProduct = {
		name : reqBody.name, 
		description : reqBody.description,
		brand : reqBody.brand,
		sku : reqBody.sku,
		stocks : reqBody.stocks,
		price : reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
		.then(product => {

			console.log("Product details updated.");

			return true;
		}).catch(err => {
			console.log(err);

			return false;
		})
};

// Archive product
module.exports.archiveProduct = (reqParams, reqBody) => { 

	console.log(reqParams.productId);
	
	return Product.findById(reqParams.productId).then(data => {

		if(data.isActive === false){	

			console.log("Product already archived.")
			return false	
		} else {		

			let toActive = {
				isActive : reqBody.isActive
			};

			return Product.findByIdAndUpdate(reqParams.productId, toActive)
				.then(product => {

					console.log("Product archived.")
					return true;
				}).catch(err => {
					console.log(err);

					return false;
				})	
		}
	}).catch(err => {
		console.log(err);

		return false;
	})
};

// Activate product
module.exports.activateProduct = (reqParams, reqBody) => { 

	console.log(reqParams.productId);
	
	return Product.findById(reqParams.productId).then(data => {

		if(data.isActive === false){	
			
			let toActive = {
				isActive : reqBody.isActive
			};

			return Product.findByIdAndUpdate(reqParams.productId, toActive)
				.then(product => {

					console.log("Product activated.")
					return true;
				}).catch(err => {
					console.log(err);

					return false;
				})				
		} else {		
	
			console.log("Product already activated.")
			return false
		}
	}).catch(err => {
		console.log(err);

		return false;
	})
};