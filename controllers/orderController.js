// JS file with JWT/JSONWebToken for user authentication
const auth = require("../auth.js");

const Order = require("../models/Order.js");
const Product = require("../models/Product.js");
const User = require("../models/User.js");

// For ObjectId
const {ObjectId} = require('mongodb');

// Non-admin user checkout
module.exports.createOrder = async (data) => {

	// Price variable
	let productPrice;

	// User existence await variable
	let isUserExists = await User.findById(data.userId).then(result => {

		if(result === null) { // User does not exist

			return false
		} else { // User exists

			return true
		}
	}).catch(err => {
		console.log(err);

		return false;
	});
	
	// Product existence await variable
	let isProductExists = await Product.findById(data.productId).then(result => {

		if(result === null) { // Product does not exist

			return false
		} else { // Product exists

			productPrice = result.price;
			return true
		}
	}).catch(err => {
		console.log(err);

		return false;
	});

	// Check if user and product exists
	if(isUserExists && isProductExists) {

		// Update variables
		let products = [{
			productId : data.productId,
			quantity : data.quantity,
			subTotal : (productPrice * data.quantity)
		}];
		let total = products.map(prop => prop.subTotal).reduce((a, b) => a + b, 0)

		// Inserting user and product on order
		let newOrder = new Order({		
			userId : data.userId,
			products : products,
			totalAmount : total

		});

		return newOrder.save().then(user => true).catch(err => {
				console.log(err);

				return false;
			})

	} else { 

		console.log("invalid user and/or product")

		return false;
	}
};

// *Retrieve authenticated user's orders
module.exports.getUserOrders = data => {

	return Order.find({ userId : data.userId }).then(result => {

		return result
	}).catch(err => {
		console.log(err);

		return false;
	});
};

// *Retrieve all orders
module.exports.getAllOrders = reqBody => {

	return Order.find({ $or: [ { status : "Pending" }, { status : "Completed" }, { status : "Cancelled" } ] }).then(data => {

		return data;
	}).catch(err => {
		console.log(err);

		return false;
	})
};

// ~Order cancellation
module.exports.cancelOrder = (reqParams, reqBody)=> {
	
	return Order.findById(reqParams.orderId).then(data => {

		if(data.status !== "Cancelled"){	
			
			let toCancel = {
				status : reqBody.status
			};

			return Order.findByIdAndUpdate(reqParams.orderId,
				toCancel)
				.then(order => {

					console.log("Order cancelled.")
					return true;
				}).catch(err => {
					console.log(err);

					return false;
				})				
		} else {		
	
			console.log("Order already cancelled.")
			return false
		}
	}).catch(err => {
		console.log(err);

		return false;
	})
};

// ~Order cancellation
module.exports.completeOrder = (reqParams, reqBody)=> {
	
	return Order.findById(reqParams.orderId).then(data => {

		if(data.status !== "Completed"){	
			
			let toComplete = {
				status : reqBody.status
			};

			return Order.findByIdAndUpdate(reqParams.orderId,
				toComplete)
				.then(order => {

					console.log("Order completed.")
					return true;
				}).catch(err => {
					console.log(err);

					return false;
				})				
		} else {		
	
			console.log("Order already completed.")
			return false
		}
	}).catch(err => {
		console.log(err);

		return false;
	})
};

// ~Retrieve product sales
module.exports.getProductSales = reqParams => {

	return Order.find({ $and: [ { 'products.productId' :  reqParams.productId }, { status : "Completed" } ] }).then(result => {

		return result
	}).catch(err => {
		console.log(err);

		return false;
	});
};



/* ======= STRETCH GOALS : CART ======= */

let products = [];
let productPrice;

// *Add to cart
// Should check if productId already exists in cart, else add quantity
// Use push() or $push to add element to embedded document products, use find()
// POST productId parameter and is only those that is owned by the logged user
module.exports.addToCart = async (userId, productId, initialQuantity) => { 

	// Check cart existence with user await variable
	let isThereCart = await Order.find({ $and : [ { userId : userId }, { status : "On Cart" } ] })
		.then(result => {

			if(!(result === null)) { // User with cart does not exist

				return false
			} else { // User with cart exists

				return true
			}
		})
		.catch(err => {

			console.log(err);

			return false;
		});

	// Check user existence await variable
	let isUserExists = await User.findById(userId)
		.then(result => {

			if(result === null) { // User does not exist

				return false
			} else { // User exists

				return Order.deleteOne({ $and : [ { userId : userId }, { purchasedOn : null }, { status : "On Cart" } ] })
					.then(result => {

						return true
					})
					.catch(err => {

						console.log(err);

						return false;
					});
			}
		}).catch(err => {
			console.log(err);

			return false;
		});
	
	// Product existence await variable
	let isProductExists = await Product.findById(productId)
		.then(result => {

			if(result === null) { // Product does not exist

				return false
			} else { // Product exists

				productPrice = result.price;
				return true
			}
		})
		.catch(err => {
			console.log(err);

			return false;
		});

	// Check if product exists in the cart already
	let isProductExistsInCart = await Order.find({ $and : [ { userId : userId }, { 'products.productId' :  productId }, { purchasedOn : null }, { status : "On Cart" } ] })
		.then(result => {

			if (result.length > 0) {

				return true
			} else {

				return false
			}
		})
		.catch(err => {

			console.log(err);

			return false;
		});

	console.log(isThereCart)

	// Check cart existence for pushing product or creating new cart
	if (isThereCart) { // If a cart already exists 

		if (isProductExistsInCart) { // Find if product exists in cart

			// Add quantity by one if product exists on cart already
			for(let i = 0; i < products.length; i++) {

				if(productId == products[i].productId) {

					products[i].quantity += 1;

					products[i].subTotal = (productPrice * products[i].quantity)
				}
			}
			 
			// Update totalAmount if product exists on cart already. This is after adding and updating quantity by one
			let newTotal = products.map(prop => prop.subTotal).reduce((a, b) => a + b, 0)

			let updateCart = {
				products : products,
				totalAmount : newTotal,
			};

			console.log(products);

			return Order.findOneAndUpdate({ $and : [ { userId : userId }, { purchasedOn : null }, { status : "On Cart" } ] }, updateCart)
				.then(product => {

					console.log("Updated the product details and total amount.")
					return true;
				})
				.catch(err => {
					console.log(err);

					return false;
				})
			
		}
		else { // If product does not exist in cart

			// Find an existing cart to push on
			return Order.find({ $and : [ { userId : userId }, { purchasedOn : null }, { status : "On Cart" } ] })
				.then(cart => {

					// New products to push
					let toCart = {
						productId : productId,
						quantity : initialQuantity,
						subTotal : (productPrice * initialQuantity)
					}

					cart.products.push(toCart);

					return cart.save().then(user => true)
						.catch(err => {

							console.log(err);
							
							return false
						})
				})
				.catch(err => {
					console.log(err);
					console.log("error in retrieving cart");

					return false;
				});
		}	
	}
	else { // If a cart doesn't exist yet, then create a new cart

		// Check if user and product exists
		if(isUserExists && isProductExists) {

			let newProduct = {
				productId : productId,
				quantity : initialQuantity,
				subTotal : (productPrice * initialQuantity)
			};
			
			products.push(newProduct);

			console.log(products);

			let total = products.map(prop => prop.subTotal).reduce((a, b) => a + b, 0)

			// Create new Order object but as Cart
			let orderCart = new Order(
				{
					userId : userId,
					products : products,
					totalAmount : total,
					purchasedOn : null,
					status : "On Cart"
				}
			);

			// Create new cart
			return orderCart.save()
				.then(user => true)
				.catch(err => {
					console.log(err);

					return false;
				})
		}
		else { 

			console.log("invalid user and/or product")

			return false;
		}
	}
};

// *Added products (Retrieve all products from cart)
// GET only those that is owned by the logged user and has status as "Cart" and purchaseOn as null
module.exports.getCart = (userId) => { 

	return Order.find({ $and: [ { userId : userId }, { purchasedOn : null }, { status : "On Cart" } ] }, { products : 1, _id : 0 })
		.then(result => {

			console.log(result);

			if (result.length > 0) {

				console.log(products)

				return result
			}
			else {

				console.log("Cart is empty");

				return false
			}
			
		})
		.catch(err => {

			console.log(err);

			return false;
		});
};

// *Change product quantities and Show(Subtotal for each item & Total price for all items)
// Should check if productId already exist in cart, else add quantity
// Use find() with operators on query, where it is owned by the logged user and status is on Cart
// PATCH productId parameter, update quantity property with body, return data with subtotal and total amount
module.exports.editCart =  async (userId, productId, newQuantity) => { 

	// Check if parameters are valid and existing
	let isValid = await Order.find({ $and: [ { userId : userId }, { 'products.productId' : productId }, { status : "On Cart" } ] })
		.then(result => {

			console.log(result);

			if(result.length > 0) {

				// Get product price
				return Product.findById(productId).then(data => {

					console.log(data.price)

					productPrice = data.price;

					return true

				}).catch(err => {
					console.log(err);

					return false;
				})
			} else {

				return false
			}
		})
		.catch(err => {

			console.log(err);

			return false;
		});

	console.log(isValid);

	// If-else cart existence
	if (isValid) {

		if( products.length > 0 ) {

			// Edit product details from products array
			for(let i = 0; i < products.length; i++) {

				if(productId == products[i].productId) {

					products[i].quantity = newQuantity;

					products[i].subTotal = (productPrice * products[i].quantity);
				}
			}
			
			console.log(products);

			// Update products embedded documents everyone
			/*
			return Order.updateOne(
				{ "_id" : ObjectId() },
				{
					$set : {
						products : products,
						totalAmount : { $sum : {'products.subTotal'} }
					}
				}
			);
			*/

			// Updating cart details
			let newTotal = products.map(prop => prop.subTotal).reduce((a, b) => a + b, 0)

			let updateCart = {
				products : products,
				totalAmount : newTotal,
			};

			return Order.findOneAndUpdate({ $and : [ { userId : userId }, { purchasedOn : null }, { status : "On Cart" } ] }, updateCart)
				.then(product => {

					console.log(product)

					console.log("Updated the product details and total amount.")
					return true;
				})
				.catch(err => {
					console.log(err);

					return false;
				})
		} else {
			console.log("An error in occured while updating. Temporary array becomes empty.");

			return false
		}
	}
	// If the validation fails
	else {

		console.log("No such record found.")

		return false;
	}	
};

// *Remove products from cart
// Should check if productId already exists in cart, else add quantity
// Use pop() or $pop to remove element to embedded document products, used find
// PATCH productId parameter and is only those that is owned by the logged user
module.exports.removeFromCart = async (userId, productId) => { 

	// Remove from products array
	/*
		if (products.length !== 0) {

			for(let i = 0; i < products.length; i++) {

			if(productId == products[i].productId) {

				products.splice(products[i], 1);
			}
		}
		else {
			
			console.log("Products cart is empty.")

			return false
		}
	*/

	// Check if parameters are valid and existing
	let isValid = await Order.find({ $and: [ { userId : userId }, { 'products.productId' : productId }, { status : "On Cart" } ] })
		.then(result => {

			console.log(result);

			if(result === null) {

				return false
			} else {

				return true
			}
		})
		.catch(err => {

			console.log(err);

			return false;
		});

	console.log(isValid);

	// If-else cart existence
	if (isValid) {

		// Find product to remove embedded product
		return Order.updateOne( { $and: [ { userId : userId }, { 'products.productId' : productId }, { status : "On Cart" } ] }, { $pull : { products : { productId: productId } } } )
			.then(result => {

				console.log("Product with id of " + productId + " has been removed from cart.")
				return true;
			})
			.catch(err => {
				console.log(err);
				console.log("Error in retrieving cart and product");

				return false;
			});
	}
	// If the validation fails
	else {

		console.log("No such record found.")

		return false;
	}
};

// ~Non-admin user cart to checkout 
// First check if user has order status of "On Cart"
// Then Change status to "Pending" and purchasedOn to new Date()
module.exports.cartToOrder = async (userId) => { 

	// Cart existence and status is "On Cart"
	let isCartExists = await Order.find({ $and: [ { userId : userId }, { status : "On Cart" } ] }).then(result => {

		if(result === null) {

			return false
		} else {

			return true
		}
	}).catch(err => {
		console.log(err);

		return false;
	}); 

	// Check if existence is true
	if(isCartExists) {

		// Updating cart as checkout order
		let toCheckout = {		
			purchasedOn : new Date(),
			status : "Pending"
		};

		return Order.findOneAndUpdate({ $and : [ { userId : userId }, { purchasedOn : null }, { status : "On Cart" } ] }, toCheckout)
			.then(product => {

				console.log("Items on cart has been checked out.");

				return true;
			}).catch(err => {
				console.log(err);

				return false;
			})

	} else { 

		console.log("No such cart found.")

		return false;
	}
};
/* ======= END OF STRETCH GOALS : CART ======= */