const jwt = require("jsonwebtoken");

const secret = "ECommerceAPI";

// Token creation
module.exports.createAccessToken = user => {

	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
	};

	return jwt.sign(data, secret, {});
};

// Token verification
module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;

	if(typeof token !== "undefined") { // Token received and is not undefined

		console.log(token);

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if(err) { // If JWT is not valid

				return res.send({ auth : "failed" });
			} else { // If JWT is valid

				next();
			}
		})
	} else { // Token does not exist

		return res.send({ auth : "failed" });
	}
};

// Token decryption
module.exports.decode = token => {

	// Token received and is not undefined
	if(typeof token !== "undefined") {

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if(err) {

				return null
			} else {

				return jwt.decode(token, { complete : true }).payload;
			}
		})
	// Token does not exist
	} else {

		return null
	}
};