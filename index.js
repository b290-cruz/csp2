// Setup dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Importing routers
const userRoute = require("./routes/userRoute.js");
const productRoute = require("./routes/productRoute.js");
const orderRoute = require("./routes/orderRoute.js");

// Server setup
const app = express();

// Database connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.5gecoq9.mongodb.net/e-commerce?retryWrites=true&w=majority",
	{ 
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

// Checking database connection
mongoose.connection.on("error", console.error.bind(console, "Connection error"));
mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas"));

// Setup middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended : true }));

// Setup routes
app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute);

// Port listening
if(require.main === module){
	app.listen(process.env.PORT || 4000, () => {
		console.log(`Ecommerce API is now online on port ${process.env.PORT || 4000}`)
	})
}

module.exports = { app, mongoose };